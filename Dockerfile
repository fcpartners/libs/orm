FROM registry.gitlab.com/fcpartners/libs/golang-build-container:latest

COPY /app /app

ENTRYPOINT ["/app"]
