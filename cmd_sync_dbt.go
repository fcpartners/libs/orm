package orm

import (
	"encoding/json"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
// DB Object
//----------------------------------------------------------------------------------------------------------------------
type dbObject struct {
	Type dbElement // Object type (db_...)
	Name string    // Object name
	Desc string    // Object desc
}

//----------------------------------------------------------------------------------------------------------------------
// DB Comment
//----------------------------------------------------------------------------------------------------------------------
type DbComment struct {
	Object string  `json:"Object"`
	Desc   *string `json:"Desc,omitempty"`
	//Logged   *bool   `json:"Logged,omitempty"`
	//Replica  *bool   `json:"Replica,omitempty"`
	Notifier *string `json:"DbEvent,omitempty"`
}

func (t *DbComment) AsJson() string {
	if t == nil {
		return "{}"
	}
	var svDesc string
	//var svLogged bool
	//var svReplica bool
	var svNotifier string

	if t.Desc == nil {
		svDesc = "" // def value
	} else {
		svDesc = *t.Desc
		if svDesc == "" {
			t.Desc = nil
		}
	}

	if t.Notifier == nil {
		svNotifier = ""
	} else {
		svNotifier = *t.Notifier
		if svNotifier == "" {
			t.Notifier = nil
		}
	}

	defer func() {
		if t.Desc == nil {
			t.Desc = new(string)
		}
		*t.Desc = svDesc
		if t.Notifier == nil {
			t.Notifier = new(string)
		}
		*t.Notifier = svNotifier
	}()

	data, err := json.Marshal(t)
	if err != nil {
		logger.Errorf("%s", err.Error())
		return ""
	}
	return string(data)
}

type dbCommentInfo struct {
	dbType    dbElement
	schema    string
	table     string
	column    string
	dbComment DbComment
}

func newDbCommentInfo(typ dbElement, mi *modelInfo) *dbCommentInfo {
	this := &dbCommentInfo{dbType: typ, schema: mi.schema, table: mi.table}
	this.dbComment.Object = mi.name // Model
	this.dbComment.Desc = nil       // Desc
	this.dbComment.Notifier = nil   // Table has notify event
	return this
}

func (t *dbCommentInfo) GetDbComment() *DbComment {
	if t != nil {
		return &t.dbComment
	}
	return nil
}

func (t dbCommentInfo) GetSQL(newSchema ...string) string {
	schema := append(newSchema, t.schema)[0]

	desc := ""
	if t.dbComment.Object != "" {
		desc = t.dbComment.AsJson()
	}

	var sql string
	switch t.dbType {
	case db_table_desc:
		sql = fmt.Sprintf(`COMMENT ON TABLE %s."%s" IS '%s';`, schema, t.table, desc)
	case db_table_column_desc:
		sql = fmt.Sprintf(`COMMENT ON COLUMN %s."%s"."%s" IS '%s';`, schema, t.table, t.column, desc)
	case db_desc:
	case db_schema_desc:
	case db_function_desc:
	case db_trigger_desc:
	}
	return sql
}
