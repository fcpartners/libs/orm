package orm

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////// Messages ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TABLE
var msgTableNotUsed = func(ti *tableInfo) {
	logger.Warnf("orm: table %s.%s more not used CRM, will be mark as unused", ti.schema, ti.table)
}

var msgTableNotUsedDrop = func(ti *tableInfo) {
	logger.Warnf("orm: TABLE %s.%s more not used CRM, will be marked as unused", ti.schema, ti.table)
}

var msgTableNotFound = func(mi *modelInfo) {
	logger.Warnf("orm: TABLE %s.%s not found, will be created", mi.schema, mi.table)
}

var msgTableLocateIncorrect = func(ti *tableInfo, md *modelInfo) {
	logger.Warnf("orm: TABLE %s.%s located by incorrect schema, will be moved to the schema %q", ti.schema, ti.table, md.schema)
}

// TABLE COLUMN
var msgColumnNotFound = func(fi *fieldInfo) {
	logger.Warnf("orm: table %s.%s, COLUMN %s not found, will be created", fi.mi.schema, fi.mi.table, fi.column)
}

var msgColumnNotUsed = func(mi *modelInfo, column string) {
	logger.Warnf("orm: table %s.%s, COLUMN %s more not used, will be dropped", mi.schema, mi.table, column)
}

var msgColumnNotUsedDelDesc = func(mi *modelInfo, column string) {
	logger.Warnf("orm: table %s.%s, COLUMN %s more not used, clearing description...", mi.schema, mi.table, column)
}

var msgColumnNotUsedDelNotNull = func(mi *modelInfo, column, colType, colNull string) {
	logger.Warnf("orm: table %s.%s, COLUMN %s %s %s more not used, dropping NOT NULL constraint......", mi.schema, mi.table, column, colType, colNull)
}

// TABLE INDEX
var msgIndexRepeated = func(mi *modelInfo, idx, ide *indexInfo) {
	logger.Errorf("orm: model %s, INDEX:%s %s(%s) is repeated, aready exists index:%s %s(%s), will be ignored...",
		mi.name,
		idx.IndexName, idx.getUniqueWord(), idx.ColumnsCommaText,
		ide.IndexName, ide.getUniqueWord(), ide.ColumnsCommaText,
	)
}

var msgIndexNotFound = func(mi *modelInfo, idx *indexInfo) {
	logger.Warnf("orm: table %s.%s, INDEX %s %s(%s) not found, will be created", mi.schema, mi.table, idx.IndexName, idx.getUniqueWord(), idx.ColumnsCommaText)
}

var msgIndexNotCorrespond = func(mi *modelInfo, idx, dbIdx *indexInfo) {
	logger.Warnf("orm: table %s.%s, INDEX %s, fields(%s) do not correspond to the described(%s), will be fixed...",
		mi.schema, mi.table, idx.IndexName, idx.ColumnsCommaText, dbIdx.ColumnsCommaText)
}

var msgIndexNotUsed = func(idx *indexInfo) {
	logger.Warnf("orm: table %s.%s, INDEX %s %s(%s) more not used, will be dropped", idx.Schema, idx.Table, idx.IndexName, idx.getUniqueWord(), idx.ColumnsCommaText)
}

// FUNCTION
var msgFunctionNotUsed = func(pi *procInfo) {
	logger.Warnf("orm: FUNCTION %s.%s more not used CRM, will be mark as unused...", pi.schema, pi.header)
}

// TRIGGERS
var msgTriggerNotUsed = func(ti *triggerInfo) {
	logger.Warnf("orm: table %s.%s, TRIGGER %s(enabled:%v) more not used CRM, will be dropped...", ti.schema, ti.table, ti.name, ti.enabled)
}
