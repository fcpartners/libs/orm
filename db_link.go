package orm

import (
	"database/sql"
	"fmt"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------- Database Links -------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------- DB Links--------------------------------------------------------------------------
var dbLinkCashe = new_dbLinks()

type dbLinks struct {
	items map[string]*DBLink
}

func new_dbLinks() *dbLinks {
	this := new(dbLinks)
	this.items = map[string]*DBLink{}
	return this
}

func (this *dbLinks) GetList() (sl []*DBLink) {
	for _, link := range this.items {
		sl = append(sl, link)
	}
	return
}

//----------------------------------- DB Link --------------------------------------------------------------------------
type DBLink struct {
	Alias     string `orm:"column(alias)"    json:"alias"   desc:"link alias"`
	Owner     string `orm:"column(owner)"    json:"owner"   desc:"link owner  db server alias"`
	Remote    string `orm:"column(remote)"   json:"remote"  desc:"link remote db server alias"`
	Objects   string `orm:"column(objects)"  json:"objects" desc:"link remote db server objects(* | table1,table2,...)"`
	srvOwner  *DBServer
	srvRemote *DBServer
}

func (this *DBLink) test() error {

	//if this.Alias == "" {
	//	return fmt.Errorf("orm: Database link alias can't be empty" )
	//}
	//
	//if this.GetPlayerActive == "" {
	//	return fmt.Errorf("orm: Database link owner server alias can't be empty" )
	//}
	//
	//if this.Remote == "" {
	//	return fmt.Errorf("orm: Database link remote server alias can't be empty" )
	//}
	//
	//this.srvOwner = DBServers.FindDBbyAlias(this.GetPlayerActive)
	//if this.srvOwner == nil {
	//	return fmt.Errorf("orm: Server with alias %q not found! must be registered before using link", this.GetPlayerActive)
	//}
	//
	//this.srvRemote = DBServers.FindDBbyAlias(this.Remote)
	//if this.srvRemote == nil {
	//	return fmt.Errorf("orm: Server with alias %q  not found! must be registered before using link", this.Remote)
	//}
	return nil
}

func (this *DBLink) supportLink(server *DBServer) error {
	al := getDbAlias(server.Alias)
	if !al.DbBaser.Support(db_link) {
		return fmt.Errorf("orm: database %s not supported database link...", al.info())
	}
	return nil
}

func (this *DBLink) dropLink(al *alias) error {

	// Start withdraw
	var tr *sql.Tx
	tr, err := al.DB.Begin()
	if err != nil {
		return err
	}
	defer tr.Rollback()

	query := fmt.Sprintf("DROP SCHEMA IF EXISTS %s CASCADE;", this.Alias)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	query = fmt.Sprintf("DROP USER MAPPING IF EXISTS FOR PUBLIC SERVER %s_srv;", this.Alias)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	query = fmt.Sprintf("DROP SERVER IF EXISTS %s_srv CASCADE;", this.Alias)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	if err := tr.Commit(); err != nil {
		return fmt.Errorf("orm: error:%v", err)
	}
	return nil
}

func (this *DBLink) createLink(al *alias) error {
	if err := this.dropLink(al); err != nil {
		return err
	}

	query := fmt.Sprintf("CREATE EXTENSION IF NOT EXISTS postgres_fdw;")
	if _, err := al.DB.Exec(query); err != nil {
		logger.Warnf("orm: error:%v [%v]", err, query)
	}

	// Start withdraw
	var tr *sql.Tx
	tr, err := al.DB.Begin()
	if err != nil {
		return fmt.Errorf("orm: error:%v", err)
	}
	defer tr.Rollback()

	query = fmt.Sprintf("CREATE SERVER %s_srv FOREIGN DATA WRAPPER postgres_fdw OPTIONS ( host '%s', dbname '%s' );", this.Alias, this.srvRemote.Host, this.srvRemote.DbName)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	query = fmt.Sprintf("CREATE USER MAPPING FOR PUBLIC SERVER %s_srv OPTIONS ( user '%s', password '%s' );", this.Alias, this.srvRemote.User, this.srvRemote.Password)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	query = fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s;", this.Alias)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	limit := ""
	if len(this.Objects) > 0 && this.Objects != "*" {
		limit = fmt.Sprintf("LIMIT TO (%s)", this.Objects)
	}

	query = fmt.Sprintf("IMPORT FOREIGN SCHEMA %s %s FROM SERVER %s_srv INTO %s;", this.srvRemote.Schema, limit, this.Alias, this.Alias)
	if _, err := tr.Exec(query); err != nil {
		return fmt.Errorf("orm: error:%v [%v]", err, query)
	}

	// Commit
	if err := tr.Commit(); err != nil {
		return fmt.Errorf("orm: error:%v", err)
	}
	return nil
}

func (this *DBLink) testLinkConnect(al *alias) (bool, error) {

	o, err := NewOrmWithDB(al.Server.Driver, al.Name, al.DB)
	if err != nil {
		return false, fmt.Errorf("orm: error:%v", err)
	}

	// test count mapping && user mapping
	query := fmt.Sprintf(`
    SELECT COUNT(*)      as Cnt
         , MAX(m.umuser) as Usr
      FROM pg_user_mapping m
           INNER JOIN pg_foreign_server s on s.oid = m.umserver  
     WHERE s.srvname = '%s_srv'`, this.Alias)

	type ti struct {
		Cnt int64
		Usr int64
	}
	dti := &ti{}

	err = o.Raw(query).QueryRow(dti)
	if err != nil {
		if err != ErrNoRows { //no row found
			return false, fmt.Errorf("orm: error:%v [%v]", err, query)
		}
	}
	// должна быть 1 запись
	if dti.Cnt != 1 {
		return false, nil
	}
	// должен быть user c id=0(PUBLIC)
	if dti.Usr > 0 {
		return false, nil
	}

	// test параметры сервера
	query = fmt.Sprintf(`
    SELECT REPLACE(s.srvoptions[1], 'host=',     '')::text AS host
         , REPLACE(s.srvoptions[2], 'dbname=',   '')::text AS dbname
         , REPLACE(m.umoptions[1],  'user=',     '')::text AS user
         , REPLACE(m.umoptions[2],  'password=', '')::text AS password
      FROM pg_foreign_server s
           JOIN pg_user_mapping m on m.umserver = s.oid
     WHERE s.srvname = '%s_srv'`, this.Alias)

	link_si := new(DBServer)
	if err = o.Raw(query).QueryRow(link_si); err != nil {
		if err != ErrNoRows { //no row found
			return false, fmt.Errorf("orm: error:%v [%v]", err, query)
		}
	}
	// параметры сервера были изменены ?
	return link_si.Host == this.srvRemote.Host && link_si.DbName == this.srvRemote.DbName, nil
}

func (this *DBLink) create() error {
	if err := this.supportLink(this.srvOwner); err != nil {
		return err
	}

	if err := this.supportLink(this.srvRemote); err != nil {
		return err
	}

	al := getDbAlias(this.srvOwner.Alias)

	// Check if link Schema exists
	ok, err := al.DbBaser.SchemaExists(al.DB, this.Alias)
	if err != nil {
		return err
	}

	// link Schema does not exists
	if !ok {
		if err := this.createLink(al); err != nil {
			return err
		}
	} else {
		// Schema exists
		// 2) testing the foreign server parameters
		if ok, err := this.testLinkConnect(al); err != nil {
			return err
		} else if !ok {
			logger.Warnf("orm: parameters of sever %s:%s:%s was changed. Re-creating link...", this.srvRemote.Driver, this.srvRemote.Host, this.srvRemote.DbName)
			if err := this.createLink(al); err != nil {
				return err
			}
		}

		// 3) check the correctness of tables links
		remote_al := getDbAlias(this.srvRemote.Alias)
		external_tables := this.Objects

		if len(external_tables) == 0 || external_tables == "*" { // Link to all tables - get all table from remote database
			// get all table list from remote server
			tables, err := remote_al.DbBaser.GetTables(remote_al.DB, remote_al.Server.Schema)
			if err != nil {
				return err
			}

			// Crete list of remote tables
			ts := make([]string, 0, len(tables))
			for _, ti := range tables {
				ts = append(ts, ti.table)
			}
			external_tables = strings.Join(ts, ",")
		}

		db_tables, err := al.DbBaser.GetTables(al.DB, this.Alias)
		if err != nil {
			return fmt.Errorf("orm: error:%v", err)
		}

		for _, table := range strings.Split(external_tables, ",") {
			// check if table exists in link schema
			fullTableName := fmt.Sprintf("%s.%s", this.Alias, table)
			ti, ok := db_tables[fullTableName]

			if !ok { // table not exists - add it
				logger.Warnf("orm: adding the foreign table %q.%s.%s --> %q.%s.%s", this.srvRemote.DbName, this.srvRemote.Schema, table, al.Server.DbName, this.Alias, table)
				query := fmt.Sprintf(`IMPORT FOREIGN SCHEMA %s LIMIT TO (%s) FROM SERVER %s_srv INTO %s;`, this.srvRemote.Schema, table, this.Alias, this.Alias)
				if _, err := al.DB.Exec(query); err != nil {
					return fmt.Errorf("orm: error:%v [%v]", err, query)
				}
				continue
			}

			// table exists check - проверяем наличие всех полей
			// get column list of owner server

			//link_columns, err := al.DbBaser.GetColumns(al.DB, this.Alias, table)
			link_columns, err := al.DbBaser.GetColumns(al.DB, ti.schema, ti.table)
			if err != nil {
				return err
			}

			// get column list of remote server
			remot_columns, err := remote_al.DbBaser.GetColumns(remote_al.DB, remote_al.Server.Schema, table)
			if err != nil {
				return err
			}

			// сравниваем список полей
			flagUpd := false
			if len(link_columns) != len(remot_columns) {
				flagUpd = true
			} else { // count of column equal - test column names
				for col := range remot_columns {
					if _, ok := link_columns[col]; !ok {
						flagUpd = true
						break
					}
				}
			}

			if flagUpd {
				// удаляем таблицу из линка и тут же ее добавляем
				logger.Warnf("orm: refreshing the foreign table %q.%s.%s --> %q.%s.%s ", this.srvRemote.DbName, this.srvRemote.Schema, table, al.Server.DbName, this.Alias, table)
				query := fmt.Sprintf(`DROP FOREIGN TABLE IF EXISTS %s.%s`, this.Alias, table)
				if _, err := al.DB.Exec(query); err != nil {
					logger.Errorf("orm: error:%v [%v]", err, query)
				}
				query = fmt.Sprintf(`IMPORT FOREIGN SCHEMA %s LIMIT TO (%s) FROM SERVER %s_srv INTO %s;`, this.srvRemote.Schema, table, this.Alias, this.Alias)
				if _, err := al.DB.Exec(query); err != nil {
					logger.Errorf("orm: error:%v [%v]", err, query)
				}
			}
			ti.used = true // используется в линке
		}

		// Remove more unused tables
		for _, ti := range db_tables {
			if !ti.used {
				logger.Warnf("orm: foreign table %q.%s.%s more not used, removing from link", al.Server.DbName, ti.schema, ti.table)
				query := fmt.Sprintf("DROP FOREIGN TABLE %s.%s;", ti.schema, ti.table)
				if _, err := al.DB.Exec(query); err != nil {
					logger.Errorf("orm: error:%v [%v]", err, query)
				}
			}
		}

	}
	return nil
}

func resetDBLinkCashe() {
	dbLinkCashe.items = map[string]*DBLink{}
	return
}

func UnRegisterDBLink(link *DBLink) {
	if _, ok := dbLinkCashe.items[link.Alias]; ok {
		delete(dbLinkCashe.items, link.Alias)
	}
	return
}

func RegisterDBLink(link *DBLink) error {
	// test parameters of link
	if link.Alias == "" {
		return fmt.Errorf("orm: Database link alias can't be empty")
	}

	if link.Owner == "" {
		return fmt.Errorf("orm: Database link owner server alias can't be empty")
	}

	if link.Remote == "" {
		return fmt.Errorf("orm: Database link remote server alias can't be empty")
	}

	logger.Infof("orm: registering link %q on %q...", link.Alias, link.Owner)

	link.srvOwner = DBServers.FindDBbyAlias(link.Owner)
	if link.srvOwner == nil {
		return fmt.Errorf("orm: Server with alias %q not found! must be registered before using link", link.Owner)
	}

	link.srvRemote = DBServers.FindDBbyAlias(link.Remote)
	if link.srvRemote == nil {
		return fmt.Errorf("orm: Server with alias %q  not found! must be registered before using link", link.Remote)
	}

	if _, ok := dbLinkCashe.items[link.Alias]; ok {
		logger.Debugf("orm: Database link %q already registered, ignoring...", link.Alias)
		return nil
	}

	// Remove spaces
	link.Objects = strings.TrimSpace(link.Objects)

	if err := link.create(); err != nil {
		return err
	}

	dbLinkCashe.items[link.Alias] = link
	return nil
}
