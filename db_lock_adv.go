package orm

import (
	"fmt"
	"sync"
	"time"
)

var Advisory = &advLocker{}

type object struct {
	Oid  int32
	Name string
	Key  int32
}

type advResponse struct {
	Error   error
	Success bool  // результат
	LockPid int64 // pid сессии, заблокировавшей обьект
	ThisPid int64 // pid сессии, делавшей запрос
	Obj     object
}

func (t *advResponse) InitObject(obj interface{}, key int64) {
	t.Obj.Oid = GetObjectOid(obj)
	t.Obj.Name = GetObjectName(obj)
	t.Obj.Key = int32(key)
}

type advLocker struct {
	al    *alias
	mutex sync.RWMutex
}

func (t *advLocker) connect() {
	t.mutex.Lock()
	defer t.mutex.Unlock()
	if t.al != nil {
		return
	}

	// find connection's *alias
	al := getDbAlias(defaultDbAlias)

	// create new connection's *alias
	t.al = newAlias()

	if dr, ok := drivers[al.Server.Driver]; ok {
		t.al.DriverType = dr
		t.al.DbBaser = dbBasers[dr]
	} else {
		panic(fmt.Sprintf("adv: db %s, driver %q have not registered", al.info(), al.Server.Driver))
	}

	t.al.Name = "adv"
	t.al.Server = al.Server
	t.al.DriverName = al.Server.Driver
	t.al.TZ = time.UTC    // Default TimeZone
	t.al.MaxOpenConns = 1 // Only one connection
	t.al.MaxIdleConns = 1 // Only one connection

	//logger.Infof("adv: connecting to %s %q %s:%d.%s(%s)",
	//	al.Server.Driver, al.Name, al.Server.Host, al.Server.Port, al.Server.DbName, al.Server.User)

	t.al.openDB() // open db with connection manager
	return
}

func (t *advLocker) askResult(res *advResponse, query string) {
	if t.al == nil {
		res.Error = fmt.Errorf("Alias is nil")
		return
	}
	if t.al.DB == nil {
		res.Error = fmt.Errorf("al.DB is nil")
		return
	}

	res.Error = t.al.DB.QueryRow(query).Scan(&res.Success, &res.LockPid, &res.ThisPid, &res.Obj.Oid, &res.Obj.Key)
	return
}

// пробует наложить исключительную блокировку на уровне сеанса для объекта(obj) с ключем(key)
func (t *advLocker) Lock(obj interface{}, key int64) (res advResponse) {
	res.InitObject(obj, key)
	t.connect()

	if !t.al.DbBaser.Support(db_lock_advisory) {
		res.Error = fmt.Errorf("adv: db %s not supported the advisory_lock", t.al.Server.Driver)
		return
	}

	query := fmt.Sprintf(`
    WITH li AS 
    ( 
      SELECT d.classid::integer      as obj_id
           , d.objid::integer        as rec_id
           , d.pid                   as back_pid
           , coalesce(pl.pid,0)      as lock_pid
        FROM ( SELECT %d               as classid
                    , %d               as objid
                    , pg_backend_pid() as pid
             ) d LEFT JOIN pg_locks pl ON pl.locktype         = 'advisory'
                                      AND pl.classid::integer = d.classid::integer
                                      AND pl.objid::integer   = d.objid::integer
      LIMIT 1 
    )
    SELECT case
             when li.lock_pid = 0                                       -- не заблокирован
             then (SELECT pg_try_advisory_lock( li.obj_id, li.rec_id )) -- результат блокировки
             else false                                                 -- уже заблокирован
           end as success
         , case
             when li.lock_pid != 0 -- заблокирован   
             then li.lock_pid
             else (SELECT pid FROM pg_locks pl 
                    WHERE pl.locktype         = 'advisory'
                      AND pl.classid::integer = li.obj_id
                      AND pl.objid::integer   = li.rec_id) 
           end as lock_pid
         , li.back_pid
         , li.obj_id
         , li.rec_id
      FROM li;`, res.Obj.Oid, res.Obj.Key)

	t.askResult(&res, query)
	logger.Debugf("adv: Lock: response:%+v", res)

	return
}

// Возвращает заблокирован ли на уровне сеанса для объекта(obj) с ключем(key)
func (t *advLocker) IsLocked(obj interface{}, key int64) (res advResponse) {
	res.InitObject(obj, key)
	t.connect()

	query := fmt.Sprintf(`
    WITH li AS 
    ( 
      SELECT d.classid::integer      as obj_id
           , d.objid::integer        as rec_id
           , d.pid                   as back_pid
           , coalesce(pl.pid,0)      as lock_pid
        FROM ( SELECT %d               as classid
                    , %d               as objid
                    , pg_backend_pid() as pid
             ) d LEFT JOIN pg_locks pl ON pl.locktype         = 'advisory'
                                      AND pl.classid::integer = d.classid::integer
                                      AND pl.objid::integer   = d.objid::integer
      LIMIT 1 
    )
    SELECT li.lock_pid::boolean as success
         , li.lock_pid
         , li.back_pid
         , li.obj_id
         , li.rec_id
      FROM li;`, res.Obj.Oid, res.Obj.Key)

	t.askResult(&res, query)
	return
}

// Освобождает исключительную блокировку на уровне сеанса для объекта(obj) с ключем(key)
func (t *advLocker) UnLock(obj interface{}, key int64) (res advResponse) {
	res.InitObject(obj, key)
	t.connect()

	query := fmt.Sprintf(`
    WITH li AS 
    ( 
      SELECT d.classid::integer      as obj_id
           , d.objid::integer        as rec_id
           , d.pid                   as back_pid
           , coalesce(pl.pid,0)      as lock_pid
        FROM ( SELECT %d               as classid
                    , %d               as objid
                    , pg_backend_pid() as pid
             ) d LEFT JOIN pg_locks pl ON pl.locktype         = 'advisory'
                                      AND pl.classid::integer = d.classid::integer
                                      AND pl.objid::integer   = d.objid::integer
      LIMIT 1 
    )
    SELECT pg_advisory_unlock( li.obj_id, li.rec_id ) as success
         , li.lock_pid
         , li.back_pid
         , li.obj_id
         , li.rec_id
      FROM li;`, res.Obj.Oid, res.Obj.Key)

	t.askResult(&res, query)
	logger.Debugf("adv: UnLock: response:%+v", res)
	return
}
