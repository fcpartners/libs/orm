package orm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/lib/pq"
	"reflect"
	"runtime"
	"sync"
	"time"
)

const (
	listener_ReconnectInterval    = 10 * time.Second
	listener_maxReconnectInterval = time.Minute
	wait_Notification             = 5 * time.Second
	wait_Reconnect                = 2 * time.Second
)

type EventOpeationType string

const (
	OP_INSERT EventOpeationType = "INSERT"
	OP_UPDATE EventOpeationType = "UPDATE"
	OP_DELETE EventOpeationType = "DELETE"
)

func (t EventOpeationType) String() string {
	return string(t)
}

type NotifyEventType struct {
	DbName       string            `json:"dbname"`
	Schema       string            `json:"schema"`
	Table        string            `json:"table"`
	Id           interface{}       `json:"id"`
	Operation    EventOpeationType `json:"operation"`
	CountChg     int               `json:"count_chg"`
	RecordData   string            `json:"data"`
	RecordIsLong bool              `json:"data_long"`
	ObjectName   string            `json:"object_name"`
	model        *modelInfo
	eventData
}

type eventData struct {
	err      error
	object   interface{}
	prepared bool
}

func (this *NotifyEventType) syncDataFieldsWithModel() {
	if this.model == nil {
		return
	}
	if this.RecordIsLong {
		return
	}

	var msgMapTemplate interface{}
	if err := json.Unmarshal([]byte(this.RecordData), &msgMapTemplate); err != nil {
		return
	}

	msgMap := msgMapTemplate.(map[string]interface{})
	msgMapNew := make(map[string]interface{})

	for column, value := range msgMap {
		fi := this.model.fields.GetByColumn(column)
		if fi == nil {
			//logger.Debugf("orm.notifier: Model %s(%s) field %q not found...ignored", this.model.name, this.model.table, column)
			continue
		}

		if fi.rel {
			nm := make(map[string]interface{})
			nm[fi.relModelInfo.fields.pk.name] = value
			value = nm
		}
		msgMapNew[fi.name] = value
	}

	data, err := json.Marshal(&msgMapNew)
	if err != nil {
		return
	}
	this.RecordData = string(data)
}

func (this *NotifyEventType) GetData() string {
	return this.prepareData().RecordData
}

func (this *NotifyEventType) GetObject() interface{} {
	this.prepareData()
	if this.eventData.object == nil {
		if this.model == nil {
			this.eventData.err = fmt.Errorf("db notifier: Unknown(nil) model for table %q", this.Table)
			return this
		}
		this.eventData.object = this.model.GetObject()
		if err := json.Unmarshal([]byte(this.RecordData), this.eventData.object); err != nil {
			this.eventData.err = fmt.Errorf("db notifier: error Unmarshal: %v[%s]", err, this.RecordData)
			this.eventData.object = nil
		}
	}
	return this.eventData.object
}

func (this *NotifyEventType) GetError() error {
	return this.prepareData().eventData.err
}

func (this *NotifyEventType) prepareData() *NotifyEventType {
	if this.eventData.prepared {
		return this
	}
	this.eventData.prepared = true

	// не пришли данные по причине превышения 8000 байт в сообщении
	if this.RecordIsLong {
		if this.model == nil {
			this.eventData.err = fmt.Errorf("db notifier: Unknown(nil) model for table %q", this.Table)
			return this
		}

		// интерфейс обьека нотификации
		this.eventData.object = this.model.GetObject()
		// присваиваем id
		Utils().Interface(this.eventData.object).Fields().SetFieldValue("Id", this.Id)
		// читаем запись из базы
		if err := this.model.Orm().SetDebug(false).Read(this.eventData.object); err != nil {
			this.eventData.err = fmt.Errorf("db notifier: error Load data from %q", this.Table)
			this.eventData.object = nil
		}
		// заполняем данные записи
		if err := json.Unmarshal([]byte(this.RecordData), this.eventData.object); err != nil {
			this.eventData.err = fmt.Errorf("db notifier: error Unmarshal: %v[%s]", err, this.RecordData)
		}
	}
	return this
}

type NotifyCallBackFuncType func(*NotifyEventType)

type _dbNotifiers map[string]*notifier // list of notifiers
var dbNotifiers = make(_dbNotifiers)

func (this _dbNotifiers) exists(dbAlias string) bool {
	_, ok := this[dbAlias]
	return ok
}

func (this _dbNotifiers) add(nt *notifier) {
	if _, ok := this[nt.al.Name]; !ok {
		this[nt.al.Name] = nt
	}
}

func (this _dbNotifiers) get(dbAlias string) *notifier {
	if nt, ok := this[dbAlias]; ok {
		return nt
	}
	return nil
}

func (this _dbNotifiers) remove(nt *notifier) {
	if nt == nil {
		return
	}
	if this.exists(nt.al.Name) {
		if nt.started {
			nt.stop()
			<-nt.quit // wait for stop
			logger.Infof("db notifier: %q database notifier stopped...", nt.al.Name)
		}
		delete(this, nt.al.Name)
	}
}

type notifier struct {
	al        *alias // структура подключения к БД
	listener  *pq.Listener
	events    map[string][]NotifyCallBackFuncType // имя прослушиваемых событий и их обработчики
	extractor map[string]bool                     // имя прослушиваемых событий и extractor type

	started bool // признак старта нотификатора
	lock    sync.Mutex
	quit    chan bool // канал остановки нотификатора
}

func newNotifier() *notifier {
	return &notifier{
		events:    make(map[string][]NotifyCallBackFuncType),
		extractor: make(map[string]bool),
		quit:      make(chan bool),
	}
}

func (this *notifier) getCallBackFunc(event string) []NotifyCallBackFuncType {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.events[event]; ok {
		return this.events[event]
	}
	return make([]NotifyCallBackFuncType, 0)
}

func (this *notifier) getExtractors(event string) bool {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.extractor[event]; ok {
		return this.extractor[event]
	}
	return false
}

func (this *notifier) removeEvent(event string, fn NotifyCallBackFuncType, selfExtractor bool) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	if !this.started {
		logger.Warnf("db notifier: %q database notifier not started!", this.al.Name)
		return nil
	}

	fns, ok := this.events[event]
	if !ok {
		return nil
	}

	found := false
	fnAddr := runtime.FuncForPC(reflect.ValueOf(fn).Pointer())
	// find call back function
	fnSlice := []NotifyCallBackFuncType{}
	for _, ff := range fns {
		if runtime.FuncForPC(reflect.ValueOf(ff).Pointer()) == fnAddr {
			found = true
			continue
		}
		fnSlice = append(fnSlice, ff)
	}

	if found {
		// if no function used this event
		if len(fnSlice) == 0 {
			// Unlisten notification
			if err := this.listener.Unlisten(event); err != nil {
				err = fmt.Errorf("db notifier: %v", err)
				return err
			}

			delete(this.events, event) // delete function registration
			if _, ok := this.extractor[event]; ok {
				delete(this.extractor, event) // delete extractor
			}

		} else {
			// other function used this event
			this.events[event] = fnSlice
		}
		logger.Infof("db notifier: %q database notifier unregistered event %q for function %v...", this.al.Name, event, fnAddr.Name())
	} else {
		logger.Infof("db notifier: %q database notifier not found registered event %q for function %v...", this.al.Name, event, fnAddr.Name())
	}

	return nil
}

func (this *notifier) addEvent(event string, fn NotifyCallBackFuncType, selfExtractor bool) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	if !this.started {
		logger.Warnf("db notifier: %q database notifier not started!", this.al.Name)
		return nil
	}

	if err := this.listener.Listen(event); err != nil {
		if err != pq.ErrChannelAlreadyOpen {
			return err
		}
	}

	if fns, ok := this.events[event]; ok {
		for _, ff := range fns {
			fn_name := runtime.FuncForPC(reflect.ValueOf(fn).Pointer())
			if runtime.FuncForPC(reflect.ValueOf(ff).Pointer()) == fn_name {
				logger.Warnf("db notifier: %q database notifier already registered event %q for function %v...", this.al.Name, event, fn_name.Name())
				return nil
			}
		}
	}

	logger.Infof("db notifier: %q database notifier registered event %q...", this.al.Name, event)
	this.events[event] = append(this.events[event], fn)
	this.extractor[event] = selfExtractor

	return nil
}

func (this *notifier) start() error {
	if this.started {
		return nil
	}

	reportProblem := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			logger.Errorf("%s", err.Error())
		}
	}
	logger.Infof("db notifier: %q database notifier starting...", this.al.Name)
	this.listener = pq.NewListener(this.al.Server.GetConnectionString(), listener_ReconnectInterval, time.Minute, reportProblem)

	this.started = true

	go func() {
		for {
			this.waitForNotification()
		}
	}()

	return nil
}

func (this *notifier) stop() {
	this.quit <- true
	this.started = false
}

func (this *notifier) waitForNotification() {
	for {
		select {
		case n := <-this.listener.Notify:
			go func() {
				if n == nil {
					return
				}

				// Prepare notification payload for pretty print
				var data bytes.Buffer
				err := json.Indent(&data, []byte(n.Extra), "", "\t")
				if err != nil {
					logger.Errorf("%s", err.Error())
					return
				}

				event := &NotifyEventType{}
				err = json.Unmarshal(data.Bytes(), event)
				if err != nil {
					logger.Errorf("%s", err.Error())
					return
				}

				if !this.getExtractors(n.Channel) {
					modelCache.RLock()
					defer modelCache.RUnlock()
					mi, ok := modelCache.get(event.Table)
					if ok {
						event.ObjectName = mi.name
						event.model = mi
						event.syncDataFieldsWithModel()
					} else {
						event.RecordData = data.String()
					}
				}

				// Call callback func
				for _, fn := range this.getCallBackFunc(n.Channel) {
					go fn(event)
				}
				//logger.Debugf("db notifier: sender(%s.%s) channel(%s) [id(%v) op(%v) changes(%d)]",
				//	this.al.Server.DbName, this.al.Server.Schema, n.Channel, event.Id, event.Operation, event.CountChg)
			}()
			return

		case <-time.After(wait_Notification):
			go func() {
				this.listener.Ping()
			}()
			return

		case <-this.quit:
			logger.Infof("db notifier: finishing listener %v...", this.al.Name)
			this.listener.UnlistenAll()
			this.listener.Close()
			this.stop()
			return
		}
	}
}

func UnRegisterNotification(dbAliases ...string) {
	if len(dbAliases) > 0 {
		for _, dbAlias := range dbAliases {
			nt := dbNotifiers.get(dbAlias)
			dbNotifiers.remove(nt)
		}

	} else {
		for _, nt := range dbNotifiers {
			dbNotifiers.remove(nt)
		}
	}
}

func registerNotification(al *alias) {
	nt := newNotifier()
	nt.al = al
	dbNotifiers.add(nt)
	nt.start()
	return
}

//func RegisterNotification(dbAlias string ) error {
//	al, ok := dataBaseCache.get(dbAlias)
//	if !ok {
//		return fmt.Errorf("db notifier: DB server info with alias `%s` not registered", dbAlias)
//	}
//
//	if !al.DbBaser.Support(db_notifier) {
//		return fmt.Errorf("db notifier: DB server info with alias `%s` not supported NOTIFY", dbAlias)
//	}
//
//	if dbNotifiers.exists(dbAlias) {
//		return nil
//	}
//	registerNotification(al)
//
//	return nil
//}

func RegisterEventByName(dbAlias string, event string, fn NotifyCallBackFuncType, selfExtract ...interface{}) error {
	if !dbNotifiers.exists(dbAlias) {

		al, ok := dataBaseCache.get(dbAlias)
		if !ok {
			return fmt.Errorf("db notifier: DB server info with alias `%s` not registered", dbAlias)
		}

		if !al.DbBaser.Support(db_notifier) {
			panic(fmt.Sprintf("db notifier: DB server: %q info with alias %q not supported NOTIFY", al.Server.Driver, al.Name))
		}

		registerNotification(al)
	}

	nt := dbNotifiers.get(dbAlias)
	if len(selfExtract) == 0 {
		nt.addEvent(event, fn, false)
	} else {
		if selfExtract[0] == nil {
			nt.addEvent(event, fn, false)
		} else if v, ok := selfExtract[0].(bool); ok {
			nt.addEvent(event, fn, v)
		} else {
			nt.addEvent(event, fn, false)
		}
	}
	return nil
}

func RegisterEventByModel(ptrStructOrTableName interface{}, fn NotifyCallBackFuncType) error {
	mi, err := getModel(ptrStructOrTableName)
	if err != nil {
		panic(err.Error())
	}

	if !mi.al.DbBaser.Support(db_notifier) {
		panic(fmt.Sprintf("db notifier: DB server: %q info with alias %q not supported NOTIFY", mi.al.Server.Driver, mi.al.Name))
	}

	if !dbNotifiers.exists(mi.al.Name) {
		if !mi.al.DbBaser.IsTriggerActive(mi.al.DB, mi.schema, mi.getEventTriggerName()) {
			panic(fmt.Sprintf("db notifier:: model %q(%s.%s) has no trigger for NOTIFY on database %q. first add `DB_table_notifier` tag to %s.TableExData() ", mi.name, mi.schema, mi.table, mi.al.Server.DbName, mi.name))
		}
		registerNotification(mi.al)
	}

	return dbNotifiers.get(mi.al.Name).addEvent(mi.getEventName(), fn, false)
}

func UnRegisterEventByModel(ptrStructOrTableName interface{}, fn NotifyCallBackFuncType) error {
	mi, err := getModel(ptrStructOrTableName)
	if err != nil {
		panic(err.Error())
	}

	nt := dbNotifiers.get(mi.al.Name)
	if nt != nil {
		return nt.removeEvent(mi.getEventName(), fn, false)
	}

	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DB Objects for db_notifier
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Postgres Event function
const pgFnTrEvent = `CREATE OR REPLACE FUNCTION public.fn_tr_event (
)
RETURNS trigger AS
$body$
DECLARE
    REC           RECORD;
    js_rec        jsonb;
    js_old        jsonb;
    field         text;
    cntchg        integer = 0;
    data          json = '{}'::json;
    data_long     boolean = false;
    notification  json;
    iter          integer = 0;
    RECD          RECORD;
BEGIN
  ----------- notify -----------------------
  IF TG_OP = 'DELETE'
    THEN REC = OLD;
    ELSE REC = NEW;
  END IF;
  js_rec = to_jsonb(REC);  -- json of current record

  IF TG_OP = 'UPDATE' THEN
    -- calc changes between old/new records
    js_old = to_jsonb(OLD); -- old record data
    FOR field IN
      SELECT jsonb_object_keys(js_old)
    LOOP
      IF COALESCE(js_old->>field,'') <> COALESCE(js_rec->>field,'') THEN
         cntchg = cntchg + 1;
      END IF;
    END LOOP;
  ELSE
    SELECT count(*) from jsonb_object_keys(js_rec) into cntchg;
  END IF;

  ------------------------------------------------------------------------------------------------------
  -- replace json datetime fields from ISO1(only timestamp types)
  -- resolve <error Unmarshal: parsing time "2021-03-27T17:29:50.397523" as "2006-01-02T15:04:05Z07:00"
  ------------------------------------------------------------------------------------------------------
  FOR RECD IN
    WITH js_keys AS ( 
      SELECT jsonb_object_keys(js_rec) as col
    ), dt_cols AS (
      SELECT column_name AS col
        FROM information_schema.Columns
       WHERE table_schema = TG_TABLE_SCHEMA
         AND table_name   = TG_TABLE_NAME
         AND udt_name = 'timestamp'
    ), jsdt AS ( 
      SELECT js_keys.col
        FROM js_keys
             JOIN dt_cols ON dt_cols.col = js_keys.col
    )
    SELECT jsdt.col AS key
         , jsonb_extract_path_text(js_rec, jsdt.col::text) || '+00:00' AS value
      FROM jsdt
  LOOP
    -- replace in json timestamp values
    js_rec = jsonb_set(js_rec, array_append(ARRAY[]::text[], RECD.key), to_jsonb(RECD.value));
  END LOOP;
  -------------------------------------------------------------------------------------------------------

  -- Construct the notification as a JSON string.
  data = jsonb_strip_nulls(js_rec); -- json of current record
  WHILE iter < 2
  LOOP
    notification = json_build_object(
            'dbname'   ,    current_database(),
            'schema'   ,    TG_TABLE_SCHEMA,
            'table'    ,    TG_TABLE_NAME,
            'id'       ,    REC.id,
            'operation',    TG_OP,
            'count_chg',    cntchg,
            'data',         data::text,
            'data_long',    data_long
        );
    if length(notification::text) < 8000 then
      EXIT;
    end if;

    --RAISE NOTICE 'data too long(%)', length(notification::text);
    data = '{}'::json;
    data_long = true;
    iter = iter + 1;
  END LOOP;

  -- Execute pg_notify(channel, notification)
  PERFORM pg_notify(TG_TABLE_SCHEMA || '.' || TG_TABLE_NAME, notification::text );
  RAISE NOTICE '%', notification;

  RETURN REC;
END;
$body$
LANGUAGE 'plpgsql'
`
