package orm

import (
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------- DB Server ------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
var DBServers = newDBServers()

type dbServers struct {
	items map[string]*DBServer
}

func newDBServers() *dbServers {
	this := new(dbServers)
	this.items = map[string]*DBServer{}
	return this
}

func (this *dbServers) Clear() {
	this.items = map[string]*DBServer{}
	return
}

func (this *dbServers) Add(servers ...*DBServer) {
	for _, server := range servers {
		if _, ok := this.items[server.Alias]; ok {
			logger.Debugf("Database server with alias `%s` already registered, skiping...", server.Alias)
			continue
		}
		this.items[server.Alias] = server
	}
	return
}

func (this *dbServers) Delete(servers ...*DBServer) {
	for _, server := range servers {
		if _, ok := this.items[server.Alias]; ok {
			delete(this.items, server.Alias)
		}
	}
	return
}

func (this *dbServers) ServerList() (sl []*DBServer) {
	for _, server := range this.items {
		sl = append(sl, server)
	}
	return
}

func (this *dbServers) FindDBbyAlias(Alias string) *DBServer {
	if val, ok := this.items[Alias]; ok {
		return val
	}
	return nil
}

func (this *dbServers) findServer(driver, dbname, schema string) *DBServer {
	for _, server := range this.items {
		if server.Driver == driver &&
			server.DbName == dbname &&
			server.Schema == schema {
			return server
		}
	}
	return nil
}

type DBServer struct {
	Alias    string `orm:"column(alias)"    json:"alias"    desc:"DB alias"`
	Driver   string `orm:"column(driver)"   json:"driver"   desc:"DB driver"     oneof:"postgres,vertica"`
	Host     string `orm:"column(host)"     json:"host"     desc:"DB host"`
	Port     int    `orm:"column(port)"     json:"port"     desc:"DB port"`
	DbName   string `orm:"column(dbname)"   json:"dbname"   desc:"DB name"`
	Schema   string `orm:"column(schema)"   json:"schema"   desc:"DB shema name"`
	User     string `orm:"column(user)"     json:"user"     desc:"DB user"`
	Password string `orm:"column(password)" json:"password" desc:"DB password"`
	SSL      string `orm:"column(ssl)"      json:"ssl"      desc:"DB SSL"        oneof:"enable,disable"`
	Odbc     bool   `orm:"column(odbc)"     json:"odbc"     desc:"is ODBC use"`
	Debug    bool   `orm:"-"`
}

//func ( this *DBServer) GetDriverName() string {
//	return this.Driver;
//}

// returns connection driver
func (this *DBServer) GetConnectionDriver() string {
	if driverType, ok := drivers[this.Driver]; ok {
		if connectionDriver, ok := dbDriver[driverType]; ok {
			return connectionDriver
		}
	} else {
		panic(fmt.Errorf("driver name `%s` have not registered", this.Driver))
	}
	return this.Driver
}

// returns connection string
func (this *DBServer) GetConnectionString() string {
	if this.SSL == "" {
		this.SSL = "disable"
	}

	if this.Driver == DBSqlite {
		return this.DbName
	}

	res := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		this.Host,
		this.Port,
		this.DbName,
		this.User,
		this.Password,
		this.SSL)

	if this.Odbc {
		res = fmt.Sprintf("Driver=%s;Servername=%s;Port=%d;Database=%s;uid=%s;pwd=%s;ConnectionLoadBalance=1",
			this.Driver,
			this.Host,
			this.Port,
			this.DbName,
			this.User,
			this.Password)
	}
	return res
}

// Check if the server matches the specified
func (this *DBServer) sameAs(srv *DBServer) bool {
	if this.Driver == srv.Driver &&
		this.Host == srv.Host &&
		this.Port == srv.Port &&
		this.DbName == srv.DbName {
		return true
	}
	return false
}

func (this *DBServer) server() DBServer {
	return DBServer{Driver: this.Driver, Host: this.Host, Port: this.Port, DbName: this.DbName}
}
