package orm

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

//var formatSlice = []string{
//	formatTimeShort    ,
//	formatTime         ,
//	formatDate         ,
//	formatDateTime     ,
//	formatDateTimeFull ,
//	formatDateTimeFuZ1 ,
//	formatDateTimeFuZ2 ,
//	formatJsonDate     ,
//	formatJsonTime     ,
//	formatJsonDateTime ,
//	formatJsonFull     ,
//	formatJsonFullZone ,
//}

var testStringSlice = []string{
	"\"2019-09-09T17:06:41.000004+03:00\"",
	"\"2020-01-17T10:42:36.189546+09:00\"",
	"\"2020-01-17T10:42:36.18954+09:00\"",
	"\"2020-01-17T10:42:36.1895+09:00\"",
	"\"2020-01-17T10:42:36.189+09:00\"",
	"\"2020-01-17T10:42:36.18+09:00\"",
	"\"2020-01-17T10:42:36.1+09:00\"",
	"\"2020-01-17T10:42:36+01:00\"",
	//"\"2019-09-09T17:06:41.000003Z03:00\"",
	//"\"2020-01-17T10:42:36.189546Z09:00\"",
	//"\"2020-01-17T10:42:36.18954Z09:00\"",
	//"\"2020-01-17T10:42:36.1895Z09:00\"",
	//"\"2020-01-17T10:42:36.189Z09:00\"",
	//"\"2020-01-17T10:42:36.18Z09:00\"",
	//"\"2020-01-17T10:42:36.1Z09:00\"",
	//"\"2020-01-17T10:42:36Z01:00\"",
	"\"2019-09-09 17:06:41.000002+03:00\"",
	"\"2020-01-17 10:42:36.189546+09:00\"",
	"\"2020-01-17 10:42:36.18954+09:00\"",
	"\"2020-01-17 10:42:36.1895+09:00\"",
	"\"2020-01-17 10:42:36.189+09:00\"",
	"\"2020-01-17 10:42:36.18+09:00\"",
	"\"2020-01-17 10:42:36.1+09:00\"",
	"\"2020-01-17 10:42:36+01:00\"",
	//"\"2019-09-09 17:06:41.000001Z03:00\"",
	//"\"2020-01-17 10:42:36.189546Z09:00\"",
	//"\"2020-01-17 10:42:36.18954Z09:00\"",
	//"\"2020-01-17 10:42:36.1895Z09:00\"",
	//"\"2020-01-17 10:42:36.189Z09:00\"",
	//"\"2020-01-17 10:42:36.18Z09:00\"",
	//"\"2020-01-17 10:42:36.1Z09:00\"",
	//"\"2020-01-17 10:42:36Z01:00\"",
	"\"2020-05-27T07:56:25.000005+03:00\"",
	"\"2020-05-27T07:56:25.000006+00:00\"",
	"\"2020-05-27T07:56:25.000007Z\"",
	"\"2020-05-27T07:56:25+00:00\"",
}

var testErrStringSlice = []string{
	//"\"2019-13-09T17:06:41.000+03:00\"",
	//"\"2020-01-17T10:42:36.18959+02:00\"",
	//"2020-05-27T07:56:25.000000Z03:00",
}

func TestTDateTime(t *testing.T) {
	timestamp := TDateTime{}
	//fmt.Printf("%v\n", timestamp)

	for _, testStr := range testStringSlice {
		err := timestamp.UnmarshalJSON([]byte(testStr))
		require.NoErrorf(t, err, "parsing %s", testStr)
		fmt.Printf("%v\n", timestamp)
		bTimestamp, err := timestamp.MarshalJSON()
		require.NoErrorf(t, err, "MarshalJSON %s", testStr)
		fmt.Printf("JSON %v\n", string(bTimestamp))
		bTimestamp, err = timestamp.UTC().MarshalJSON()
		require.NoErrorf(t, err, "UTC MarshalJSON %s", testStr)
		fmt.Printf("UTC JSON %v\n", string(bTimestamp))
	}

	//for _, testErrStr := range testErrStringSlice{
	//	err := timestamp.UnmarshalJSON([]byte(testErrStr))
	//	require.Errorf(t, err, "parsing %s", testErrStr)
	//}

}

func TestReBuildTime(t *testing.T) {
	a := ReBuildTime("\"2020-01-17T10:42:36.18345+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17 10:42:36.18345Z02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17 10:42:36.18345Z\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17 10:42:36.18345+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17T10:42:36.18345Z02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17T10:42:36.183455+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17T10:42:36.18345536254+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17T10:42:36.183+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17T10:42:36+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2020-01-17 10:42:36+02:00\"")
	t.Log(a)
	a = ReBuildTime("\"2019-09-09 17:06:41.000000+03:00\"")
	t.Log(a)
}
