package orm

import (
	"fmt"
	"go.uber.org/zap"
	"log"
)

var logger logging

type logging struct {
	logger *zap.Logger
	set    bool
}

const (
	mod     = "ORM"
	errMsg  = "[ERROR] "
	warnMsg = "[WARN] "
	infoMsg = "[INFO] "
	dbgMsg  = "[DEBUG] "
)

func (lo logging) Errorf(f string, v ...interface{}) {
	if lo.set {
		lo.logger.Error(fmt.Sprintf(f, v...), zap.String("component", mod))
	} else if Debug {
		log.Printf(mod+errMsg+f, v...)
	}
}

func (lo logging) Warnf(f string, v ...interface{}) {
	if lo.set {
		lo.logger.Warn(fmt.Sprintf(f, v...), zap.String("component", mod))
	} else if Debug {
		log.Printf(mod+warnMsg+f, v...)
	}
}

func (lo logging) Infof(f string, v ...interface{}) {
	if lo.set {
		lo.logger.Info(fmt.Sprintf(f, v...), zap.String("component", mod))
	} else if Debug {
		log.Printf(mod+infoMsg+f, v...)
	}
}

func (lo logging) Debugf(f string, v ...interface{}) {
	if lo.set {
		lo.logger.Debug(fmt.Sprintf(f, v...), zap.String("component", mod))
	} else if Debug {
		if len(v) == 0 {
			log.Print(mod + dbgMsg + f)
		} else {
			log.Printf(mod+dbgMsg+f, v...)
		}
	}
}
