package orm

// exception map for objects url
type ObjModelExcept struct {
	Url string
}

var exceptObjMap = map[string]ObjModelExcept{
	"UserParams": {
		Url: "/crm_api/cached/",
	},
}
