// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package orm

import (
	"sync"
)

const (
	odCascade                = "cascade"
	odSetNULL                = "set_null"
	odSetDefault             = "set_default"
	odDoNothing              = "do_nothing"
	defaultStructTagName     = "orm"
	defaultStructTagDelim    = ";"
	jsonStructTagName        = "json"   // tag name for front
	jsonStructTagDelim       = ","      // separator of data tag name for front
	loggedStructTagName      = "logged" // tag logged
	descriptionStructTagName = "desc"   // tag description
)

var (
	modelCache = &_modelCache{
		cache:           make(map[string]*modelInfo),
		cacheByFullName: make(map[string]*modelInfo),
		cacheByModel:    make(map[string]*modelInfo),
	}
)

// model info collection
type _modelCache struct {
	sync.RWMutex    // only used outsite for bootStrap
	orders          []string
	cache           map[string]*modelInfo // [db_table]*modelInfo
	cacheByFullName map[string]*modelInfo // [PkgPath.modelName()]*modelInfo
	done            bool
	cacheByModel    map[string]*modelInfo // [modelName()]*modelInfo
}

// get all model info
func (mc *_modelCache) all() map[string]*modelInfo {
	m := make(map[string]*modelInfo, len(mc.cache))
	for k, v := range mc.cache {
		m[k] = v
	}
	return m
}

// get orderd model info
func (mc *_modelCache) allOrdered(al *alias, includeVirtualTable bool) []*modelInfo {
	//m := make([]*modelInfo, 0, len(mc.orders))
	//for _, table := range mc.orders {
	//	m = append(m, mc.cache[table])
	//}

	// get table for only db of alias
	m := make([]*modelInfo, 0, 1)
	for _, table := range mc.orders {
		mi := mc.cache[table]
		if mi.al == al {
			if mi.virtual && !includeVirtualTable {
				continue
			}
			m = append(m, mi)
		}
	}
	return m
}

// get model info by table name
func (mc *_modelCache) get(table string) (mi *modelInfo, ok bool) {
	mi, ok = mc.cache[table]
	return
}

// get model info by full name
func (mc *_modelCache) getByModelName(name string) (mi *modelInfo, ok bool) {
	mi, ok = mc.cacheByModel[name]
	return
}

// get model info by full name
func (mc *_modelCache) getByFullName(name string) (mi *modelInfo, ok bool) {
	mi, ok = mc.cacheByFullName[name]
	return
}

// set model info to collection
func (mc *_modelCache) set(table string, mi *modelInfo) *modelInfo {
	mii := mc.cache[table]
	mc.cache[table] = mi
	mc.cacheByFullName[mi.fullName] = mi
	mc.cacheByModel[mi.name] = mi
	if mii == nil {
		mc.orders = append(mc.orders, table)
	}
	return mii
}

// clean all model info.
func (mc *_modelCache) clean() {
	mc.orders = make([]string, 0)
	mc.cache = make(map[string]*modelInfo)
	mc.cacheByFullName = make(map[string]*modelInfo)
	mc.done = false
}

// ResetModelCache Clean model cache. Then you can re-RegisterModel.
// Common use this api for test case.
func resetModelCache() {
	modelCache.clean()
}

func Reset() {
	resetModelCache()
	resetDBLinkCashe()
	resetDatabaseCache()
}
