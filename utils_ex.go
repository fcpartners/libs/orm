package orm

import (
	"encoding/json"
	"errors"
	"fmt"
	"hash/fnv"
	"reflect"
	"regexp"
	"strconv"
)

func replaceDoubleSpace(src string) (dst string, err error) {
	dst = src
	r, err := regexp.Compile("[\\s]+")
	if err != nil {
		return
	}
	dst = r.ReplaceAllString(src, " ")
	return
}

func toJson(obj interface{}) string {
	bytes, err := json.Marshal(obj)
	if err != nil {
		return err.Error()
	}
	return string(bytes)
}

// return OID of object
func GetObjectName(value interface{}) string {
	var s string

	if value == nil {
		value = "nil"
	}

	switch v := value.(type) {
	case string:
		s = v
	case []byte:
		s = string(v)
	case bool:
		s = strconv.FormatBool(v)
	case float32:
		s = strconv.FormatFloat(float64(v), 'f', 2, 64)
	case float64:
		s = strconv.FormatFloat(v, 'f', 2, 64)
	case int:
		s = strconv.FormatInt(int64(v), 10)
	case int8:
		s = strconv.FormatInt(int64(v), 10)
	case int16:
		s = strconv.FormatInt(int64(v), 10)
	case int32:
		s = strconv.FormatInt(int64(v), 10)
	case int64:
		s = strconv.FormatInt(v, 10)
	case uint:
		s = strconv.FormatUint(uint64(v), 10)
	case uint8:
		s = strconv.FormatUint(uint64(v), 10)
	case uint16:
		s = strconv.FormatUint(uint64(v), 10)
	case uint32:
		s = strconv.FormatUint(uint64(v), 10)
	case uint64:
		s = strconv.FormatUint(v, 10)
	default:
		//s = reflect.ValueOf(value).Type().String()
		typ := indirectType(reflect.TypeOf(value))
		s = typ.Name()
		if s == "" {
			s = reflect.ValueOf(value).Type().String()
		}
	}
	return s
}

func HashByText(s string) int32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	sum := int32(h.Sum32())
	if sum < 0 {
		sum = -sum
	}
	return sum
}

// return OID of object
func GetObjectOid(obj interface{}) int32 {
	return HashByText(GetObjectName(obj))
}

//----------------------------- Interface newEngine -----------------------------------------------------------------------
func Utils() *_utils {
	return &_utils{}
}

type _utils struct{}

func (t *_utils) Interface(obj interface{}) *_interface {
	return &_interface{obj}

}

type _interface struct {
	object interface{}
}

func (t *_interface) Fields() *_fields {
	return &_fields{t}
}

type _fields struct {
	owner *_interface
}

func (t *_fields) findFieldByName(i interface{}, name string) (*reflect.StructField, *reflect.Value, error) {

	// Find field in struct
	pt := reflect.TypeOf(i)
	pv := reflect.ValueOf(i)
	if pt.Kind() == reflect.Ptr {
		pt = pt.Elem()
		pv = pv.Elem()
	}

	for i := 0; i < pv.NumField(); i++ {
		ft := pt.Field(i)
		fv := pv.Field(i)

		// если встроенная структура - ищем в ней
		if fv.Kind() == reflect.Struct && ft.Tag.Get("json") == "" {
			if ft, fv, err := t.findFieldByName(fv.Addr().Interface(), name); err == nil {
				return ft, fv, nil
			}
		} else {
			if name == ft.Name || name == ft.Tag.Get("json") {
				return &ft, &fv, nil
			}
		}
	}

	return nil, nil, errors.New(fmt.Sprintf("field %s not found ", name))
}

func (t *_fields) GetFieldValue(field_name string) interface{} {
	if t.owner.object == nil {
		return nil
	}
	if _, fv, err := t.findFieldByName(t.owner.object, field_name); err == nil {
		return fv.Interface()
	}
	return nil
}

func (t *_fields) SetFieldValue(field_name string, value interface{}) {
	if _, fv, err := t.findFieldByName(t.owner.object, field_name); err == nil {
		if fv.Kind() == reflect.Ptr && reflect.ValueOf(value).Kind() != reflect.Ptr {
			*fv = fv.Elem()
		}
		fv.Set(reflect.ValueOf(value).Convert(fv.Type()))
	}
}
